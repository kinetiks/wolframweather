# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# Config
account = '' # Your gmail account xxx@gmail.com
password = '' # gmail password for login
recipient = account # change if you want to send the email to different address
appID = ''      # wolfram app ID
max_attempts = 2 # Query attempts to WolframAlpha (2000 queries per month is the cap)
smtpServer = 'smtp.gmail.com:587'

# Import wolframalpha module and initiate client
import wolframalpha
client = wolframalpha.Client(appID)

# Queries (podindex: '3' returns History & Forecast pod)
param = dict(format='plaintext,image', podindex= '3')
def fetch(query):
    attempts = 0
    while attempts < max_attempts:
        attempts += 1
        print('WolframAlpha Query: \'{0}\' (attempt {1} out of {2})'.format(query, attempts, max_attempts))
        fetched = client.query(query, param)
        if fetched.tree.getroot().attrib.get('numpods') == '1':
            print('Fetch for \'{0}\' successful'.format(query))
            return fetched
        else:
            print('Fetch \'{0}\' failed'.format(query))
    print('Could not fetch data after {0} attempts, skipping fetch for \'{1}\''.format(attempts, query))
                
rovaniemi = fetch('temperature Rovaniemi Finland')
oulu = fetch('temperature Oulu Finland')

# Get image sources - Check http://products.wolframalpha.com/api/explorer.html
#   first [0] accesses the requested History & Forecast pod 
#   second [0] accesses first Subpod 
#   [1] accesses <img> tag
#   Get the source of img tag and save it
def get_image_source(query):
    return query.tree.getroot()[0][0][1].attrib.get('src')


# Insert image source data to email template from file
f = open('template.html', 'r', encoding="utf-8")
html = f.read().format(get_image_source(rovaniemi),get_image_source(oulu))
f.close()

# Create message container
msg = MIMEMultipart('alternative')
msg['Subject'] = "Weather Forecast for Rovaniemi and Oulu"
msg['From'] = account
msg['To'] = recipient
msg.attach(MIMEText(html, 'html'))

# Send the message via gmail SMTP server
s = smtplib.SMTP(smtpServer)
s.starttls()
s.login(account,password)
s.sendmail(msg['From'], [msg['To']], msg.as_string())
print('Email sent to {0}'.format(msg['To']))
s.quit()
